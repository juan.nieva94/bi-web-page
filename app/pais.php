<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pais extends Model
{
/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'paises';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estado','nombre',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id','created_at','updated_at',
    ];
    
    protected $with = array('provincias');

    public function provincias()
    {
        return $this->hasMany('App\provincia', 'pais_id', 'id');
    }
    
    //protected $appends = ['totalventas'];
    public function getTotalventasAttribute()
    {
        $totalVentas=0;
        foreach($this->provincias as $prov){
            foreach($prov->ciudades as $ciudad){
            foreach($ciudad->sucursales  as $suc){
            foreach(venta::all() as $venta){
            if($venta->sucursal_id == $suc->id){
                $totalVentas += $venta->montoCompra;
            }
            }
        }
        }
        }
        
        
    return $totalVentas;
    }
}
