<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class librosCompra extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'libroscompras';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Cantidad','libro_id','venta_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at',
    ];
    
     protected $with = array('libros');
    public function libros()
    {
        return $this->hasMany('App\libro','id','libro_id');
    }
}
