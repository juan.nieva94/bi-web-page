<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sucursal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sucursales';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estado','nombre',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'id','created_at','updated_at','direccion','ciudad_id',
    ];
    protected $with = array('empleados');
    public function empleados()
    {
        return $this->hasMany('App\empleado', 'sucursal_id', 'id');
    }
    //protected $appends = ['totalventas'];
    public function getTotalventasAttribute()
    {
        $totalVentas=0;
        foreach(venta::all() as $venta){
            if($venta->sucursal_id == $this->attributes['id']){
                $totalVentas += $venta->montoCompra;
                
            }
        }
    return $totalVentas;
    }
}
