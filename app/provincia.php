<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class provincia extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provincias';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estado','nombre',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pais_id','id','created_at','updated_at',
    ];
    protected $with = array('ciudades');

   public function ciudades()
    {
        return $this->hasMany('App\ciudad', 'provincia_id', 'id');
    }
    public function pais()
    {
        return $this->belongsTo('App\pais','id');
    }
    //protected $appends = ['nombreProvincia'];
    public function getNombreProvinciaAttribute(){
    return $this->attributes['nombre'];
    }
    //protected $appends = ['totalventas'];
    public function getTotalventasAttribute()
    {
        $totalVentas=0;
        foreach($this->ciudades as $ciudad){
            foreach($ciudad->sucursales  as $suc){
            foreach(venta::all() as $venta){
            if($venta->sucursal_id == $suc->id){
                $totalVentas += $venta->montoCompra;
            }
            }
        }
        }
        
        
    return $totalVentas;
    }
}
