<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class venta extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ventas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'montoCompra','empleado_id','sucursal_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at',
    ];
     protected $with = array('librosCompras');
    public function librosCompras()
    {
        return $this->hasMany('App\librosCompra','venta_id','id');
    }
}
