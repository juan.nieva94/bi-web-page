<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class libro extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'libros';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo','autor','precioUnitario','id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'created_at','updated_at',
    ];
}
