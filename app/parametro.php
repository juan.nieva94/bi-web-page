<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parametro extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'parametros';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'atributo','optimo','regular','id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at',
    ];
}
