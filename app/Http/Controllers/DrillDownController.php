<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\DB;  

class DrillDownController extends Controller
{
    public $estado="";
    public $valorCampo="";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
   
    public function index()
    {
    
        $paises = App\pais::all();
        $provincias = App\provincia::all();
        $ciudades = App\ciudad::all();
        $sucursales = App\sucursal::all();
        $empleados = App\empleado::all();
        
        $parametros = App\parametro::all();
        
        
        $libros = App\libro::all();
        $ventas = App\venta::all();
        $libroCompras = App\librosCompra::all();
        
        foreach(App\empleado::all() as $empleado){
            $empleado->estado=$this->estadoEmpleado($empleado);
            $empleado->save();
        }
    
        $estadoPais ="success";
        foreach(App\pais::all() as $Pais){
            $estadoProvincia ="success";
            foreach($Pais->provincias as $Provincia){
                $estadoCiudad ="success";
                foreach($Provincia->ciudades as $Ciudad){
                    $estadoSucursal ="success";
                    foreach($Ciudad->sucursales as $Sucursal){
                        $estadoEmpleado ="success";
                        foreach($Sucursal->empleados as $Empleado){
                            $estadoEmpleado=$this->verificarEstado($Empleado,$estadoEmpleado);
                        }
                        $Sucursal->estado=$estadoEmpleado;
                        $Sucursal->save();
                        $estadoSucursal=$this->verificarEstado($Sucursal,$estadoSucursal);
                    }
                    $Ciudad->estado=$estadoSucursal;
                    $Ciudad->save();
                    $estadoCiudad=$this->verificarEstado($Sucursal,$estadoCiudad);
                }
                $Provincia->estado=$estadoCiudad;
                $Provincia->save();
                $estadoPais=$this->verificarEstado($Provincia,$estadoPais);
            } 
            $Pais->estado=$estadoPais;
            $Pais->save();
        }
        
        $array1=App\pais::all();
        $array= $array1->toArray();
        //echo var_dump($array);
        $cadenaLista= $this->crearArrayNested($array,true,0);
       
        
        return View('home',["cadena"=>$cadenaLista,"parametros"=>App\parametro::all()[0]]);
    }
    
    private function verificarEstado($elem, $estado){
        if($estado == "success" && ($elem->estado == "danger" || $elem->estado == "warning")){
            $estado=$elem->estado;
        }
        if($estado=="warning" && $elem->estado == "danger"){
            $estado=$elem->estado;
        }
        return $estado;
    }
    
   /* 
    private function crearArrayNested($array,$first,$count){
        foreach($array as $key => $elem){
        if(!is_array($elem)){
            echo $key;echo '<br>';
            echo $count;echo '<br>';
        }
        
            else{
                $this->crearArrayNested($elem,false,$count+1);
                
                
            }
        }
        return;
    } */
    private function crearArrayNested($array,$first,$count){
        $out = "";
        if(!$first || !is_array($array)){
             $out = "<ul class='list-style' data-toggle='collapse' >";
         }
        
    foreach($array as $key => $elem){
        if(!is_array($elem)){
           if($this->valorCampo!=""){
               if($key=='totalventas'){
                   echo 'asdasd';
               }
               else{
               $out .= "<li class='list-group-item list-group-item-$elem' ><span>$this->valorCampo</span></li>";
               $this->valorCampo="";}
           }else{
               $this->valorCampo=$elem;
           }
        }
        else $out .= $this->crearArrayNested($elem,false,$count+1);
    }
        if(!$first){
            $out .= "</ul>";
         }
        
        return $out;
    } 
        
        
        
    private function estadoEmpleado($empleado){
        
        $parametros = App\parametro::all();
        $optimo = 0;
        $regular = 0;
        $totalVentas = 0;
        foreach($parametros as $par){
            if($par->atributo=="empleados"){
                $regular = $par->regular;
                $optimo = $par->optimo;
            }
        }
        
        $CONSULTARAW= DB::select('SELECT SUM( `libroscompras`.`cantidad` * `libros`.`precioUnitario`) AS `Total Vendido`
                                    FROM `libros`
                                        LEFT JOIN `libroscompras` ON `libroscompras`.`libro_id` = `libros`.`id`
                                        LEFT JOIN `ventas` ON `libroscompras`.`venta_id` = `ventas`.`id`
                                        LEFT JOIN `empleados` ON `ventas`.`empleado_id` = `empleados`.`id`
                                    WHERE (`empleados`.`id`  = '.$empleado->id.')
                                    ');
        
        $arrayVendido = (array)$CONSULTARAW;
        //
        foreach($CONSULTARAW as $reg){
            foreach($reg as $valor){
                 $totalVentas = $valor;
                }
            }
        
        
        if($totalVentas>=$optimo){
            
            return "success";
        }else{
            if($totalVentas>=$regular){
                
                return "warning";
            }else{
                
                return "danger";
            }
        }
        
        
    }
           
}