<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App;
use Table;
use Illuminate\Support\Facades\DB;  

use Illuminate\Database\Eloquent\ModelNotFoundException;

class TablasController extends Controller
{
    public function index(){
        return View('listaTablas');
    }
    public function ventas()
    {
        
        $rows= DB::select('SELECT `empleados`.`nombre` AS `Empleado`, `ventas`.`created_at` AS `Fecha de Venta`, `ciudades`.`nombre` AS `Ciudad`, (`libroscompras`.`cantidad` * `libros`.`precioUnitario`) AS `Monto Total`
FROM `libros`
    LEFT JOIN `libroscompras` ON `libroscompras`.`libro_id` = `libros`.`id`
    LEFT JOIN `ventas` ON `libroscompras`.`venta_id` = `ventas`.`id`
    LEFT JOIN `empleados` ON `ventas`.`empleado_id` = `empleados`.`id`
    LEFT JOIN `sucursales` ON `empleados`.`sucursal_id` = `sucursales`.`id`
    LEFT JOIN `ciudades` ON `sucursales`.`ciudad_id` = `ciudades`.`id`
ORDER BY  `Fecha de Venta` DESC');
        
        
        $rowsPreparedToTable=array();
        
        //echo var_dump($rows);
        
        foreach($rows as $row){
            if($row->Empleado!=NULL)
            {
                array_push($rowsPreparedToTable,$row);
            }
        }
        
        $table = Table::create($rowsPreparedToTable);
        
        return View('tablas',['table' => $table,'tipoTabla'=>'Ventas Realizadas']);   
    }
    
    public function empleados()
    {
       $rows= DB::select('SELECT `empleados`.`nombre` AS `Empleado`, `sucursales`.`nombre` AS `Sucursal`, `provincias`.`nombre` AS `Provincia`, `paises`.`nombre` AS `Pais`,(`libros`.`precioUnitario` * `libroscompras`.`cantidad`) as `Total Vendido` 
       FROM `libros` 
       LEFT JOIN `libroscompras` ON `libroscompras`.`libro_id` = `libros`.`id` LEFT JOIN `ventas` ON `libroscompras`.`venta_id` = `ventas`.`id` LEFT JOIN `empleados` ON `ventas`.`empleado_id` = `empleados`.`id` LEFT JOIN `sucursales` ON `empleados`.`sucursal_id` = `sucursales`.`id` LEFT JOIN `ciudades` ON `sucursales`.`ciudad_id` = `ciudades`.`id` LEFT JOIN `provincias` ON `ciudades`.`provincia_id` = `provincias`.`id` LEFT JOIN `paises` ON `provincias`.`pais_id` = `paises`.`id` ORDER BY `empleados`.`id`');
        
        $rowsPreparedToTable=array();
        
        //echo var_dump($rows);
        
        foreach($rows as $row){
            if($row->Empleado!=NULL)
            {
                array_push($rowsPreparedToTable,$row);
            }
        }
        
        $table = Table::create($rowsPreparedToTable);
        
        return View('tablas',['table' => $table,'tipoTabla'=>'Empleados de todas las sucursales']);   
    }
    
    public function libros()
    {$rows= DB::select(' 
       SELECT `libros`.`titulo` AS `Titulo`, `libros`.`autor` AS `Autor`, `libros`.`precioUnitario` AS `Precio Unitario`
FROM `libros`

        ');
        
        
        $table = Table::create($rows);
      return View('tablas',['table' => $table,'tipoTabla'=>'Libros']);    
    }
    
    
}
