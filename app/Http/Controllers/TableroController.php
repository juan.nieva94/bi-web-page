<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  
use Illuminate\Support\Facades\Response;
use App;

class TableroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $CONSULTARAW= DB::select('SELECT `ciudades`.`nombre`  AS `label`, COUNT(`ciudades`.`nombre`) AS `value` 
        FROM `empleados`
        LEFT JOIN `ventas` ON `ventas`.`empleado_id` = `empleados`.`id`
        LEFT JOIN `sucursales` ON `empleados`.`sucursal_id` = `sucursales`.`id`
        LEFT JOIN `ciudades` ON `sucursales`.`ciudad_id` = `ciudades`.`id`
        GROUP BY `ciudades`.`nombre`
        ');
        
        $out = array();
        $last;
        $count=1;
        
        foreach($CONSULTARAW as $reg){
            foreach($reg as $valor){
                if($count%2==0){
                    array_push($out,array($last=>$valor));
                        $count=1;
                }else{
                    $last=$valor;
                    $count++;
                }
            }
        }
        
        
        $json_ventasProvincia = json_encode($out); 
        
        $json_ventasProvincia = str_replace("{","[",$json_ventasProvincia);
        $json_ventasProvincia = str_replace("}","]",$json_ventasProvincia);
        $json_ventasProvincia = str_replace(":",",",$json_ventasProvincia);
        
        $ciudades = array();
        foreach(App\ciudad::all() as $ciudad){
            $ciudades["C".$ciudad->id] = $ciudad->nombre;
        }
        
        $sucursales = array();
        foreach(App\sucursal::all() as $sucursal){
           $sucursales["S".$sucursal->id] = $sucursal->nombre;
        }
        
        
       
        
        
        return View('tablero',['ventasPorProvincia'=>$json_ventasProvincia,'ciudades'=>$ciudades,'sucursales'=>$sucursales]);
    
    }
    
    public function listar(Request $request)
     {
         if($request->ajax()){  
             $busqueda= $request->input('busqueda');
             
             $arr = $this->multiexplode(array("C","S"),$busqueda);
             
             $arrParametros = array();
             if($arr[1]=="S"){//Sucursales
                 foreach(App\parametro::all() as $A){
                    if($A->atributo == "sucursales_ventas"){
                        $arrParametros = array('valReg'=>$A->regular,'valOpt'=>$A->optimo,'valObj'=>$A->maximoObjetivo);
                    }
                }
             }else{//Ciudades
                 foreach(App\parametro::all() as $A){
                    if($A->atributo == "ciudad_ventas"){
                        $arrParametros = array('valReg'=>$A->regular,'valOpt'=>$A->optimo,'valObj'=>$A->maximoObjetivo);
                    }
                 }
             }
             
             $datos = $this->obtenerValores($arr);
             array_push($datos,$arrParametros);
             $aux = json_encode($datos);
             
             $aux = str_replace("{","[",$aux);
             $aux = str_replace("}","]",$aux);
             $aux = str_replace(":",",",$aux);
             
             
             
             return response()->json($aux);
         }
     }
    private function obtenerValores($arr){
        if($arr[1]=="S"){
            $SQLRAW=DB::select("SELECT SUM((`libros`.`precioUnitario` * `libroscompras`.`cantidad`) )AS `Ventas Totales`
                                FROM `sucursales`
                                    LEFT JOIN `empleados` ON `empleados`.`sucursal_id` = `sucursales`.`id`
                                    LEFT JOIN `ventas` ON `ventas`.`empleado_id` = `empleados`.`id`
                                    LEFT JOIN `libroscompras` ON `libroscompras`.`venta_id` = `ventas`.`id`
                                    LEFT JOIN `libros` ON `libroscompras`.`libro_id` = `libros`.`id`
                                WHERE (`sucursales`.`id` =".$arr[0].")");
            
            return $SQLRAW;
            
        }else{
            
            $SQLRAW=DB::select("SELECT SUM((`libros`.`precioUnitario` * `libroscompras`.`cantidad`)) AS `Ventas Totales` FROM `ciudades` LEFT JOIN `sucursales` ON `sucursales`.`ciudad_id` = `ciudades`.`id` LEFT JOIN `empleados` ON `empleados`.`sucursal_id` = `sucursales`.`id` LEFT JOIN `ventas` ON `ventas`.`empleado_id` = `empleados`.`id` LEFT JOIN `libroscompras` ON `libroscompras`.`venta_id` = `ventas`.`id` LEFT JOIN `libros` ON `libroscompras`.`libro_id` = `libros`.`id` WHERE (`ciudades`.`id` =".$arr[0].")");
            
            return $SQLRAW;
        }
    }
    private function multiexplode ($delimiters,$string) {
        $Delimiter="";
        foreach($delimiters as $deli){
            if(strspn($string,$deli)>0){
                $Delimiter=$deli;
            }
        }
        $res = explode($Delimiter, $string)[1];
    return  array($res,$Delimiter);
}

    
    

}
