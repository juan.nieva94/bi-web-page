<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;


use Illuminate\Support\Facades\DB;  
class ParametrosController extends Controller
{
    public function editarParametros(){
         return view('editarParametros');
    }
    public function guardarParametros(Request $request){
       $miVector=array();
        if ($request->isMethod('post'))
        {
            array_push($miVector,array('valReg'=>$request->input('valReg'), 'valOpt'=>$request->input('valOpt'),'valObj'=>'0','indice'=>1));
            $valorRegular =$request->input('valReg');
            $valorOptimo = $request->input('valOpt');
            
            array_push($miVector,array('valReg'=>$request->input('valRegCiudad'), 'valOpt'=>$request->input('valOptCiudad'), 'valObj'=>$request->input('valObjetivoCiudad'),'indice'=>2));
            $valorRegularCiudad =$request->input('valRegCiudad');
            $valorOptimoCiudad = $request->input('valOptCiudad');
            
            array_push($miVector,array('valReg'=>$request->input('valRegSucursal'), 'valOpt'=>$request->input('valOptSucursal'), 'valObj'=>$request->input('valObjetivoSucursal'),'indice'=>3));
            $valorRegularProvincia =$request->input('valRegProvincia');
            $valorOptimoProvincia = $request->input('valOptProvincia');
            
        }
        //echo print_r($miVector);
        
        $parametrosCambiar = array();
        foreach($miVector as $conjunto){
            if($conjunto["valReg"] != '' && $conjunto["valOpt"]!='' && $conjunto["valObj"]!='' && is_numeric($conjunto["valReg"])  &&is_numeric($conjunto["valOpt"]) &&is_numeric($conjunto["valObj"]) ){
                array_push($parametrosCambiar, $conjunto);
            }
        }
        if(count($parametrosCambiar)==0){
            return redirect('editarParametros')->with('status', 'Error, campos vacios!');;
        }else{
            foreach($parametrosCambiar as $par){
                foreach(App\parametro::all() as $A){
                    if($A->id ==$par['indice']){
                        $A->regular=floatval($par['valReg']);
                        $A->optimo=floatval($par['valOpt']);
                        $A->maximoObjetivo=floatval($par['valObj']);
                        $A->save();
                    }
                }
            }
        }
        
        return View('guardarParametros');
        
    }
}
