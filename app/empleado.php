<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class empleado extends Model
{
    
    
    private $bool=true;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'empleados';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estado','nombre',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id','created_at','updated_at','documento','sucursal_id',
    ];
    public function ventas()
    {
        return $this->hasMany('App\venta', 'empleado_id','id');
    }
    
    public function sucursal()
    {
        return $this->belongsTo('App\sucursal','id');
    }
    //protected $appends = ['totalventas'];
    public function getTotalventasAttribute()
    {
        $totalVentas=0;
        foreach(venta::all() as $venta){
            if($venta->empleado_id == $this->attributes['id']){
                $totalVentas += $venta->montoCompra;
                
            }
        }
    return $totalVentas;
    }
   
}
