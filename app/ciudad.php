<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ciudad extends Model
{/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ciudades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'estado','nombre',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'provincia_id','id','created_at','updated_at','codigoPostal',
    ];
    
    protected $with = array('sucursales');
    public function sucursales()
    {
        return $this->hasMany('App\sucursal', 'ciudad_id', 'id');
    }
    //protected $appends = ['totalventas'];
    public function getTotalventasAttribute()
    {
        $totalVentas=0;
        foreach($this->sucursales  as $suc){
            foreach(venta::all() as $venta){
            if($venta->sucursal_id == $suc->id){
                $totalVentas += $venta->montoCompra;
            }
            }
        }
        
        
    return $totalVentas;
    }
    
    }
