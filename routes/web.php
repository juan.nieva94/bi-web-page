<?php
use App\pais;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home','DrillDownController@index');
Route::get('/tablero','TableroController@index');
Route::post('/tablero/','TableroController@listar');

Route::get('/listasTablas','TablasController@index');
    Route::get('/ventas','TablasController@ventas');
    Route::get('/empleados','TablasController@empleados');
    Route::get('/libros','TablasController@libros');

Route::get('/editarParametros','ParametrosController@editarParametros');
Route::post('/guardarParametros','ParametrosController@guardarParametros');