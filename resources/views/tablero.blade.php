@extends('layouts.app')
@section('content')

<script src="{{URL::to('/js/raphael.min.js')}}"></script>
<script src="{{URL::to('/js/morris.min.js')}}"></script>

<!-- Load c3.css -->
<link href="{{URL::to('/css/c3.css')}}" rel="stylesheet" type="text/css">

<!-- Load d3.js and c3.js -->
<script src="{{URL::to('/js/d3.min.js')}}" charset="utf-8"></script>
<script src="{{URL::to('/js/c3.min.js')}}"></script>

<meta charset=utf-8 />
<div class="container">
    @if(!Auth::guest())
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tablero</div>
                <div class="panel-body"> <div>
                    <div>Ingresos:
                        {{ Form::open(array('url' => '/tablero')) }}
                        {{Form::select('Filtro', array('Ciudades' => $ciudades,'Sucursales' =>  $sucursales ), 'L', array('id' => 'busqueda') ) }}
                        
                        {{Form::close()}}
                    </div><div id="informacion" class="informacion">
                        </div>
                    <div id="chartAJAX">
                    </div>
                    Ventas por ciudad:
                    <div id="ventasProvinciaChart"></div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<script>
    $(document).ready(function(){
        var chart1 = c3.generate({
        bindto: '#ventasProvinciaChart',
        data: {
        columns: {!!$ventasPorProvincia!!},
        type : 'donut',
        },
        donut: {
            title: "Ventas por ciudades"
        }
    });
    
    var chart=null;
        /*
        var chart=$("#chartAJAX");
        var valorBusqueda=$('#busqueda').val();
        var route = "{{URL::to('/tablero/lista/')}}";
        var ruta=route+"/"+valorBusqueda;
        $.get(route, function(res){
        });*/
        function changeChart(value){
            //console.log(JSON.parse(value));
            var valores = JSON.parse(value);
            console.log(valores[0][0], valores[0][1])   //Ventas Totales
            console.log(valores[1][0], valores[1][1])    //Valores Regulares
            console.log(valores[1][2], valores[1][3])    //Valores Optimos
            console.log(valores[1][4], valores[1][5])    //Valores Objetivo
            
            var ventasTotales = [valores[0][0], valores[0][1]];
            var valoresRegulares = valores[1][1];
            var valoresOptimos =  valores[1][3];
            var valoresObjetivo =  valores[1][5];
            console.log(ventasTotales);
            $('#informacion').empty().append("<h4><span class='label label-info'>Valores de ventas que superan la espectativa:"+valores[1][5]+"$ </span></h4><h4><span class='label label-info'>Valores de ventas optimos:"+valoresOptimos+"$ </span></h4><h4><span class='label label-info'>Valores de ventas regulares:"+valores[1][1]+"$</span></h4>");
            if(chart!==null){
                chart.destroy();
                chart=null;
                //console.log(chart)
                                 }
            chart = c3.generate({
        bindto: '#chartAJAX',
        data: {
            columns: [
                ["Ventas Totales",0]       
            ],
            type: 'gauge',
        },        transition: {
            duration: 500
        },
        gauge: {
            label: {
                format: function(value, ratio) {
                    return value;
                },
                show: true // to turn off the min/max labels.
            },
            min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
            max: valoresObjetivo, // 100 is default
            units: '$',
            width: 50 // for adjusting arc thickness
        },
        color: {
            pattern: ['#FF0000', '#F97600','#F97600', '#F6C600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
            threshold: {
                unit: 'value', // percentage is default
                max: 200, // 100 is default
                values: [0,valoresRegulares,valoresOptimos,valoresObjetivo]
            }
        },
        size: {
            height: 180
        }
    });
            setTimeout(function () {
                if(chart!==null){
                    chart.load({
        columns: [ventasTotales]
    });
                }
    
}, 10);
            
            
                
        }
        $( "select" ).change(function () {
            var token = $("[name='_token']").val();
            var chart=$("#chartAJAX");
            var valorBusqueda=$('#busqueda').val();
            var route = "{{URL::to('/tablero/')}}";
            $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',
                    data:{busqueda: valorBusqueda},

                    success:function(msj){
                        console.log(msj);
                         changeChart(msj);
                         
                       
                    },
                    error:function(msj){
                       console.log("No");
                    }
                });
        }).change();
    }
                     );
    
    
    
    
</script>
@endsection