@extends('layouts.app') @section('content')
<div class="container">
    @if(!Auth::guest())
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Parametros</div>
                <div class="panel-body">
                    {{ Form::open(array('url' => '/guardarParametros')) }} {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="container-fliud">
                            <div class="row">
                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo1"><span class=" glyphicon glyphicon-wrench" aria-hidden="true"></span>Editar parametros del Drill Down</button>
                                <div id="demo1" class="collapse">
                                    <div class="panel-heading">Editar parametros del Drill Down</div>{{Form::label('label1', 'Valores regulares')}} {!! Form::number('valReg',null,['class' => 'form-control','step'=>'any']) !!} {{Form::label('label2', 'Valores optimos')}} {!! Form::number('valOpt',null,['class' => 'form-control','step'=>'any']) !!}
                                </div>
                            </div>


                            <div class="row">
                                <!--- <div class="col-sm-6"> -->
                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3"><span class=" glyphicon glyphicon-wrench " aria-hidden="true"></span> Ventas por ciudad</button>
                                <div id="demo3" class="collapse">
                                    <div class="panel-heading">Editar parametros de las ventas por ciudad</div>{{Form::label('label3', 'Valores regulares ')}} {!! Form::number('valRegCiudad',null,['class' => 'form-control','step'=>'any']) !!} {{Form::label('label4', 'Valores optimos')}} {!! Form::number('valOptCiudad',null,['class' => 'form-control','step'=>'any']) !!}{{Form::label('label4', 'Objetivo')}} {!! Form::number('valObjetivoCiudad',null,['class' => 'form-control','step'=>'any']) !!}
                                </div>
                            </div>


                            <div class="row">
                                <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo4"><span class=" glyphicon glyphicon-wrench " aria-hidden="true"></span> Ventas por sucursal</button>
                                <div id="demo4" class="collapse">
                                    <div class="panel-heading">Editar parametros de las ventas por sucursal</div>{{Form::label('label3', 'Valores regulares ')}} {!! Form::number('valRegSucursal',null,['class' => 'form-control','step'=>'any']) !!} {{Form::label('label4', 'Valores optimos')}} {!! Form::number('valOptSucursal',null,['class' => 'form-control','step'=>'any']) !!}{{Form::label('label4', 'Objetivo')}} {!! Form::number('valObjetivoSucursal',null,['class' => 'form-control','step'=>'any']) !!}
                                </div>
                            </div>

                            {{Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar Valores', array('type' => 'submit', 'class' => 'btn btn-default'))}} {{ Form::close() }}
                            <div class="row">

                                @if (session('status'))
                                <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
                                </div>
                                @endif
                            </div>
                        </div>




                    </div>
                </div>
            </div>
            @endif
        </div>
        @endsection