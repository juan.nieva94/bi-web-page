@extends('layouts.app') @section('content')
    <div class="container">
        @if(!Auth::guest())
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Drill Down</div>
                        <div class="panel-body">
                            <div class="list-group list-group-root well">
                                <h3><span class="label label-info">Valores de ventas optimos: {{$parametros->optimo}}$</span></h3>
                                <h3><span class="label label-info">Valores de ventas regulares: {{$parametros->regular}}$</span></h3>
                                <div id="ads" class="panel">
                                    {!!$cadena!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        </div>
 
@endsection
