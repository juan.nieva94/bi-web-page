@extends('layouts.app')
@section('content')
<div class="container">
    @if(!Auth::guest())
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$tipoTabla}}</div>
                <div class="panel-body">
                    <div id="pop_div">
                        <a class="btn btn-info" href="{{ URL::previous() }}">Volver</a>
                    </div>
                    {!! $table->render() !!}
            </div>
        </div>
    </div>
    @endif
</div>
@endsection