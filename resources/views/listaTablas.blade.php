@extends('layouts.app') @section('content')
<div class="container">
    @if(!Auth::guest())
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tablas del sistema</div>
                <div class="panel-body">
                    <ul>
                        <li><a href="{{ url('/ventas') }}">Ventas</a></li>

                        <li><a href="{{ url('/empleados') }}">Empleados</a></li>

                        <li><a href="{{ url('/libros') }}">Libros</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endif
    </div>
    @endsection