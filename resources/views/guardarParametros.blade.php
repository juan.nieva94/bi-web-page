@extends('layouts.app') @section('content')
<div class="container">
    @if(!Auth::guest())
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Parametros </div>
                <div class="panel-body">
                    <div class="alert alert-success">
                        Los cambios se han registrado de forma correcta!

                    </div>
                    <!--
{{ Form::open(['url' => '/home', 'method' => 'get']) }}
                     {{Form::submit('Volver al Drill Down')}}
                {{ Form::close() }}
-->



                    <script type="text/javascript">
                        window.setTimeout(function() {
                            window.location.href = "{{URL::to('/home')}}";
                        }, 1500);
                    </script>

                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection